import QtQuick 2.0
import Ubuntu.Components 0.1

import Valves 1.0

Rectangle {
    id: field
    anchors.centerIn: parent
    color: "transparent"

    signal createField(int fieldWidth, int fieldHeight);
    signal clearField;
    signal fieldCreated(int fieldWidth, int fieldHeight);
    signal updateValveState(int row, int column, int stateValue);
    signal buttonClicked(int row, int column);

    property Component __component

    property int __buttonX: 0
    property int __buttonY: 0

    property int __topLeftCornerX: 0
    property int __topLeftCornerY: 0

    //property int __fieldWidth: 0
    //property int __fieldHeight: 0

    property int __buttonWidth: units.dp(25)
    property int __buttonHeight: units.dp(25)
    property int __buttonRightMargin: units.dp(2)
    property int __buttonBottomMargin: units.dp(2)

    //property var buttons: []

    function createButton(x, y)
    {
        __buttonX = x;
        __buttonY = y;

        if(__component == null)
            __component = Qt.createComponent("GameButton.qml");

        if(__component != null)
            continueButtonCreation();
        else
            __component.ready.connect(continueButtonCreation);
    }

    function continueButtonCreation()
    {
        var button = __component.createObject(field, {"row": __buttonY, "column": __buttonX,
                                                  "x": __topLeftCornerX + __buttonX * __buttonWidth + __buttonX * __buttonRightMargin,
                                                  "y": __topLeftCornerY + __buttonY * __buttonHeight + __buttonY * __buttonBottomMargin,
                                                  "width": __buttonWidth, "height": __buttonHeight});

         if (button == null) {
             // Error Handling
             console.log("Error creating object");

             return;
         }


        //field.buttons.push(button);
    }

    function destroyButtons()
    {
        console.log("Destroing buttons")

        clearField();
        /*var button = field.buttons.pop();
        while(button)
        {
            button.destroy(0);
            //destroyObject(button);
            button = buttons.pop();
        }*/
    }

    Grid
    {
        id: grid
        anchors.centerIn: parent
        rows: 4
        columns: 4
        spacing: __buttonRightMargin

        signal updateButtonState(int row, int column, int valveState);

        Repeater
        {
            id: buttons

            signal updateButtonState(int row, int column, int valveState);

            GameButton
            {
                id: gameButton
                row: Math.floor(index / __gameFieldWidth)
                column: index % __gameFieldWidth
                width: __buttonWidth
                height: __buttonHeight

                onButtonClicked:
                {
                    field.buttonClicked(row, column);
                }
            }

            onUpdateButtonState:
            {
                buttons.itemAt(row * __gameFieldWidth + column).stateUpdated(row, column, valveState);
            }
        }

        onUpdateButtonState:
        {
            buttons.updateButtonState(row, column, valveState);
        }
    }

    onCreateField:
    {
        /*destroyButtons();

        width = __buttonWidth * fieldWidth + __buttonRightMargin * (fieldWidth - 1);
        height = __buttonHeight * fieldHeight + __buttonBottomMargin * (fieldHeight - 1);

        __topLeftCornerX = 0;//-width / 2;
        __topLeftCornerY = 0;//-height / 2;

        for(var i=0;i<fieldWidth;i++)
        {
            for(var j=0;j<fieldHeight;j++)
            {
                createButton(i, j);
            }
        }*/
        width = __buttonWidth * fieldWidth + __buttonRightMargin * (fieldWidth - 1);
        height = __buttonHeight * fieldHeight + __buttonBottomMargin * (fieldHeight - 1);

        __topLeftCornerX = 0;//-width / 2;
        __topLeftCornerY = 0;//-height / 2;

        grid.rows = fieldHeight;
        grid.columns = fieldWidth;
        buttons.model = fieldWidth * fieldHeight;

        fieldCreated(fieldWidth, fieldHeight);
    }

    onFieldCreated:
    {
        valves.startGame(fieldWidth, fieldHeight);
    }

    onButtonClicked:
    {
        console.log(width + " " + height)
        valves.turn(row, column);
    }

    onUpdateValveState:
    {
        grid.updateButtonState(row, column, stateValue);
    }

    Connections
    {
        target: valves

        onValveStateUpdated:
        {
            field.updateValveState(row, column, stateValue);
        }
    }
}
