import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1

Rectangle {
    id: mainMenu
    anchors.fill: parent
    color: "transparent"

    signal startGame(int width, int height);
    signal gameSelector();
    signal highscores;
    signal information;
    signal quit;

    Column {
        anchors.centerIn: parent
        width: units.gu(40)
        spacing: units.gu(2)

        Button {
            objectName: "continueBtn"
            width: parent.width
            //enabled: false

            text: "Continue"

            onClicked: {
                startGame(16, 16);
            }
        }
        Button {
            objectName: "newGameBtn"
            width: parent.width

            text: "New Game"

            onClicked: {
                gameSelector();
            }
        }
        Button {
            objectName: "highscoreBtn"
            width: parent.width

            text: "Highscore"

            onClicked: {
                highscores();
            }
        }
        Button {
            objectName: "informationBtn"
            width: parent.width

            text: "How to play"

            onClicked: {
                information();
            }
        }
        Button {
            id: quitBtn
            objectName: "quitBtn"
            width: parent.width
            text: "Exit"

            onClicked: {
                confirmExitDialog.confirmed.connect(quit);
                confirmExitDialog.show(quitBtn);
            }
        }
    }

    ConfirmationDialog
    {
        id: confirmExitDialog
    }
}
