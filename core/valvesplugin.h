#ifndef VALVESPLUGIN_H
#define VALVESPLUGIN_H

#include <QtQml/QQmlExtensionPlugin>
#include <QtQuick/QQuickItem>

class ValvesPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
public:
    explicit ValvesPlugin(QQuickItem *parent = 0);
    
    void registerTypes(const char *uri);

    void initialize();
    void deinitialize();
};

#endif // VALVESPLUGIN_H
