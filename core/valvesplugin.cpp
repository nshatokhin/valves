#include "valves.h"
#include "valvesplugin.h"

ValvesPlugin::ValvesPlugin(QQuickItem *parent) :
    QQmlExtensionPlugin(parent)
{
}

void ValvesPlugin::registerTypes(const char *uri)
{
    // @uri mymodule
    qmlRegisterType<Valves>(uri, 1, 0, "Valves");
}

void ValvesPlugin::initialize()
{

}

void ValvesPlugin::deinitialize()
{

}
