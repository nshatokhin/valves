#ifndef VALVES_H
#define VALVES_H

#include <QTimer>
#include <QtQuick/QQuickItem>

class Valves : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(qreal atmosphere READ atmosphere WRITE setAtmosphere NOTIFY atmosphereChanged)
    Q_PROPERTY(int state READ state WRITE setState NOTIFY gameStateChanged)

public:
    explicit Valves(QQuickItem *parent = 0);
    ~Valves();

    enum STATES {
        MAIN_MENU = 0,
        IN_GAME,
        GAME_PAUSED,
        GAME_WON,
        GAME_LOST,
        INTRO,
        OUTRO,
        CREDITS
    };

public:    // Property setters and getters
    qreal atmosphere();
    void setAtmosphere(qreal atmosphere);
    int state();
    void setState(int state);

private:
    int fieldWidth;
    int fieldHeight;
    int ** field;

    QTimer * timer;

    int flashCount;
    int flashTimeToStart;
    qreal atmosphereLevel;

    int gameState;

    void changeNeighbours(int row, int column);

    void changeCross(int row, int column);
    void changeNearest(int row, int column);
    void changeDiagonal(int row, int column);
    void changeDiagonalByState(int row, int column);

    void createField(int width, int height);
    void destroyField();

    void randomizeValves();

    void changeValveState(int row, int column);
    int getValveState(int row, int column);

    bool isVictory();

signals:    // Property signals
    void atmosphereChanged(qreal atmosphereLevel);
    void gameStateChanged(int state);
    
signals:
    void startGame(int width, int height);
    void gameStarted();
    void stopGame();
    void gameStoped();
    void turn(int row, int column);
    void victory();
    void lose();
    void flash();

    void valveStateUpdated(int row, int column, int stateValue);
    
private slots:
    void onStartGame(int width, int height);
    void onStopGame();
    void onTurn(int row, int column);
    void onFlash();

private:

    Q_DISABLE_COPY(Valves)
};

#endif // VALVES_H
