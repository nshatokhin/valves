import QtQuick 2.0

Rectangle {
    id: root
    width: 100
    height: 100
    color: "blue"

    property bool checked: false

    signal mouseOver;
    signal mouseOut;
    signal clicked;

    MouseArea
    {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

        onClicked:
        {
            root.clicked();
        }

        onEntered:
        {
            if(!checked)
                root.color = "yellow"
        }

        onExited:
        {
            if(!checked)
                root.color = "blue"
        }
    }

    onCheckedChanged:
    {
        if(checked)
            root.color = "green"
        else
            root.color = "blue"
    }
}
