#include <QDebug>

#include "random_utils.h"
#include "valves.h"

Valves::Valves(QQuickItem *parent) :
    QQuickItem(parent)
{
    field = NULL;
    fieldWidth = 0;
    fieldHeight = 0;

    gameState = MAIN_MENU;

    flashTimeToStart = 5;

    timer = new QTimer();
    timer->setInterval(flashTimeToStart * 1000);

    connect(this, SIGNAL(startGame(int,int)), SLOT(onStartGame(int,int)));
    connect(this, SIGNAL(stopGame()), SLOT(onStopGame()));
    connect(this, SIGNAL(turn(int,int)), SLOT(onTurn(int,int)));
    connect(timer, SIGNAL(timeout()), this, SLOT(onFlash()));
}

Valves::~Valves()
{
    delete timer;
    destroyField();
}

void Valves::createField(int width, int height)
{
    fieldWidth = width;
    fieldHeight = height;

    if(field == NULL)
    {
        field = new int * [width];

        for(int i=0;i<width;i++)
        {
            field[i] = new int[height];
        }
    }
}

void Valves::destroyField()
{
    if(field != NULL)
    {
        for(int i=0;i<fieldWidth;i++)
        {
            delete[] field[i];
        }

        delete[] field;

        field = NULL;
    }
}

void Valves::randomizeValves()
{
    for(int i=0;i<fieldWidth;i++)
    {
        for(int j=0;j<fieldHeight;j++)
        {
            field[i][j] = binary_rand();

            emit valveStateUpdated(i, j, field[i][j]);
        }
    }
}

void Valves::onStartGame(int width, int height)
{
    qDebug() << "startGame:" << width << "," << height;

    if(width != fieldWidth || height != fieldHeight)
    {
        destroyField();
        createField(width, height);
    }

    randomizeValves();

    atmosphereLevel = 1.0;
    emit atmosphereChanged(atmosphereLevel);

    flashCount = fieldWidth * fieldHeight;

    timer->start();

    emit gameStarted();
}

void Valves::onTurn(int row, int column)
{
    qDebug() << "Clicked:" << row << "," << column;

    changeNeighbours(row, column);

    if(isVictory())
    {
        emit stopGame();
        emit victory();
    }
}

void Valves::changeNeighbours(int row, int column)
{
    changeCross(row, column);
    //changeNearest(row, column);
    //changeDiagonal(row, column);
    //changeDiagonalByState(row, column);
}

void Valves::changeCross(int row, int column)
{
    for(int i=0;i<fieldHeight;i++)
        changeValveState(row, i);

    for(int i=0;i<row;i++)
        changeValveState(i, column);

    for(int i=row+1;i<fieldWidth;i++)
        changeValveState(i, column);
}

void Valves::changeNearest(int row, int column)
{
    for(int i=row-1;i<=row+1;i++)
    {
        if(i >= 0 && i < fieldWidth)
        {
            for(int j=column-1;j<=column+1;j++)
            {
                if(j >= 0 && j < fieldHeight)
                {
                    changeValveState(i, j);
                }
            }
        }
    }
}

void Valves::changeDiagonal(int row, int column)
{
    int startRow = 0, startCol = 0;
    if(row > column)
        startRow = row - column;
    else
        startCol = column - row;

    for(int i=0;(startRow+i)<fieldWidth && (startCol+i)<fieldHeight;i++)
    {
        changeValveState(startRow + i, startCol + i);
    }

    startRow = row + column;

    for(int i=row-1, j=column+1;i>=0 && j<fieldHeight;i--, j++)
    {
        changeValveState(i, j);
    }

    for(int i=row+1, j=column-1;i<fieldWidth && j>=0;i++, j--)
    {
        changeValveState(i , j);
    }
}

void Valves::changeDiagonalByState(int row, int column)
{
    int startRow = 0, startCol = 0;

    if(getValveState(row, column) == 1)
    {
        if(row > column)
            startRow = row - column;
        else
            startCol = column - row;

        for(int i=0;(startRow+i)<fieldWidth && (startCol+i)<fieldHeight;i++)
        {
            changeValveState(startRow + i, startCol + i);
        }
    }
    else
    {
        startRow = row + column;

        for(int i=row-1, j=column+1;i>=0 && j<fieldHeight;i--, j++)
        {
            changeValveState(i, j);
        }

        for(int i=row, j=column;i<fieldWidth && j>=0;i++, j--)
        {
            changeValveState(i , j);
        }
    }
}

void Valves::changeValveState(int row, int column)
{
    if(field[row][column] == 0)
        field[row][column] = 1;
    else
        field[row][column] = 0;

    emit valveStateUpdated(row, column, field[row][column]);
}

int Valves::getValveState(int row, int column)
{
    return field[row][column];
}

bool Valves::isVictory()
{
    int sum = 0;

    for(int i=0;i<fieldWidth;i++)
    {
        for(int j=0;j<fieldHeight;j++)
        {
            sum += field[i][j];
        }
    }

    return (sum == fieldWidth * fieldHeight);
}

void Valves::onFlash()
{
    emit flash();

    atmosphereLevel -= (qreal) 1/flashCount;

    emit atmosphereChanged(atmosphereLevel);

    if(atmosphereLevel <= 0)
    {
        emit stopGame();
        emit lose();
    }
}

void Valves::onStopGame()
{
    timer->stop();

    emit gameStoped();
}

qreal Valves::atmosphere()
{
    return atmosphereLevel;
}

void Valves::setAtmosphere(qreal atmosphere)
{
    atmosphereLevel = atmosphere;
}

int Valves::state()
{
    return gameState;
}

void Valves::setState(int state)
{
    gameState = state;

    emit gameStateChanged(gameState);
}

QML_DECLARE_TYPE(Valves)
