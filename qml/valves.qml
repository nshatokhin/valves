import QtQuick 2.0
import Ubuntu.Components 0.1

import Valves 1.0
/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"
    applicationName: "valves"
    id: mainView

    signal showMenu()

    width: units.gu(100)
    height: units.gu(75)

    PageStack
    {
        id: rootStack

        Component.onCompleted:
        {
            rootStack.push(mainMenu)
        }

        Page
        {
            id: mainMenu
            title: "Valves"
            visible: false

            MainMenu
            {
                id: mainMenuInstance

                onStartGame:
                {
                    rootStack.push(game)

                    gameInstance.startGame(width, height);
                }

                onGameSelector:
                {
                    rootStack.push(gameSelector)
                }

                onHighscores:
                {
                    rootStack.push(highScores)
                }

                onInformation:
                {
                    rootStack.push(information)
                }

                onQuit:
                {
                    Qt.quit();
                }
            }
        }

        Page
        {
            id: game
            title: ""
            visible: false

            Game
            {
                id: gameInstance
                onBack:
                {
                    rootStack.pop();
                }
            }

            /*tools: ToolbarActions {
                        back {
                            itemHint: Button {
                                id: cancelButton
                                text: "cancel"
                            }
                        }
                        lock: true
                        active: true
                    }*/
        }

        Page
        {
            id: gameSelector
            title: ""
            visible: false

            GameSelector
            {
                id: selectorInstance

                onBack:
                {
                    rootStack.pop();
                }

                onStartGame:
                {
                    rootStack.pop();
                    mainMenuInstance.startGame(width, height);
                }
            }
        }

        Page
        {
            id: highScores
            title: "Highscores"
            visible: false

            HighScores
            {
                onBack:
                {
                    rootStack.pop();
                }
            }
        }

        Page
        {
            id: information
            title: "How to play"
            visible: false

            Information
            {
                onBack:
                {
                    rootStack.pop();
                }
            }
        }
    }

    Valves {
        id: valves
    }

    onShowMenu:
    {
        valves.state = 0;
        rootStack.pop();
    }
}
