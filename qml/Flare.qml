import QtQuick 2.0

Rectangle {
    id: flare
    anchors.fill: parent
    color: "white"
    opacity: 0

    signal show;
    signal hide;
    signal showed;
    signal hided;

    NumberAnimation
    {
         id: showingAnimation
         target: flare
         properties: "opacity"
         from: 0
         to: 1.0
         duration: 1000

        onRunningChanged:
        {
            if(!showingAnimation.running)
                showed();
        }
    }

    NumberAnimation
    {
         id: hidingAnimation
         target: flare
         properties: "opacity"
         from: 1.0
         to: 0
         duration: 1000

         onRunningChanged:
         {
             if(!hidingAnimation.running)
                 hided();
         }
    }


    onShow:
    {
        showingAnimation.start();
    }

    onHide:
    {
        hidingAnimation.start();
    }
}
