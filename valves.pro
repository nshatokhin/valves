#-------------------------------------------------
#
# Project created by QtCreator 2013-04-07T23:41:00
#
#-------------------------------------------------

QT       += core gui widgets qml quick

TARGET = valves

TEMPLATE = app

include(core/core.pri)

SOURCES += src/main.cpp

OTHER_FILES += qml/valves.qml \
    qml/MainMenu.qml \
    qml/Information.qml \
    qml/HighScores.qml \
    qml/Game.qml \
    qml/ConfirmationDialog.qml \
    qml/GameField.qml \
    qml/GameButton.qml \
    qml/LevelCompleteDialog.qml \
    qml/GameSelector.qml \
    qml/GraphicCheckButton.qml \
    qml/LoadingScreen.qml \
    qml/Flare.qml

RESOURCES += \
    valves.qrc
