import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    color: "black"
    visible: false

    signal show;
    signal hide;

    Text {
        id: loadingText
        anchors.centerIn: parent
        text: qsTr("Loading")
        color: "green"
        font.pointSize: 48
    }

    onShow:
    {
        visible = true;
    }

    onHide:
    {
        visible = false;
    }
}
