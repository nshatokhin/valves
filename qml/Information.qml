import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    anchors.fill: parent
    color: "transparent"

    signal back;

    Button {
                x: units.gu(1)
                y: units.gu(1)
                //iconSource: "icon.png"
                //iconPosition: "center"
                text: "Back"
                color: "green"
                onClicked: back();
            }
}
