import QtQuick 2.0

import Ubuntu.Components 0.1

import Valves 1.0

Rectangle {
    id: gameScreen
    anchors.fill: parent
    color: "black"

    signal back;
    signal startGame(int width, int height);
    signal loadingStart;
    signal loadingFinish;

    signal showFlash;
    signal hideFlash;

    property int __gameFieldWidth: 0
    property int __gameFieldHeight: 0

    Repeater
    {
         anchors.fill: parent
         model: 200

         Rectangle
         {
             x: Math.random() * parent.width
             y: Math.random() * parent.height
             height: 1
             width: 1
             color: "white"
         }
    }

    Rectangle
    {
         id: sun
         x: -(parent.width - 0.1*parent.width)
         y: parent.height/2-height/2
         width: parent.width>parent.height?parent.width:parent.height
         height: width
         color: "yellow"
         border.color: "black"
         border.width: 1
         radius: width*0.5
    }

    Rectangle
    {
         id: atmosphere
         anchors.centerIn: parent
         width: 0
         height: width
         color: Qt.rgba(0, 0.58, 1)
         border.color: "black"
         border.width: 1
         radius: width*0.5
         opacity: 1.0
    }

    Rectangle
    {
         id: crust
         anchors.centerIn: parent
         width: 0
         height: width
         color: "#4D280F"
         border.color: "black"
         border.width: 1
         radius: width*0.5
    }

    Rectangle
    {
         id: mantle
         anchors.centerIn: parent
         width: 0
         height: width
         color: "#7F0000"
         border.color: "#7F0000"
         border.width: 1
         radius: width*0.5
    }

    Rectangle
    {
         id: core
         anchors.centerIn: parent
         width: 0
         height: width
         color: "yellow"
         border.color: "yellow"
         border.width: 1
         radius: width*0.5
    }

    GameField
    {
        id: gameField

        onFieldCreated:
        {
            core.width = Math.sqrt(Math.pow(gameField.width, 2) + Math.pow(gameField.height, 2));
            mantle.width = core.width + 4 / __gameFieldWidth * core.width;
            crust.width = mantle.width + 0.4 /  __gameFieldWidth * mantle.width;
            atmosphere.width = crust.width + 2 / __gameFieldWidth * crust.width;
        }
    }

    LevelCompleteDialog
    {
        id: levelCompleteDialog
        title: "You won!"
        text: "You successfully completed the level"
    }

    LevelCompleteDialog
    {
        id: levelFailDialog
        title: "You lost!"
        text: "You lost the level"
    }

    onStartGame:
    {
        console.log("start game")

        __gameFieldWidth = width;
        __gameFieldHeight = height;

        loadingStart();
        valves.gameStarted.connect(loadingFinish);
        gameField.createField(width, height);
    }

    onLoadingStart:
    {
        console.log("Start loading")
        loadingScreen.show();
    }

    onLoadingFinish:
    {
        console.log("Finish loading")
        loadingScreen.hide();
    }

    onShowFlash:
    {
        console.log("flash")

        flare.show();
    }

    onHideFlash:
    {
        console.log("flash ended")

        flare.hide();
    }

    Button
    {
        x: units.gu(1)
        y: units.gu(1)
        //iconSource: "icon.png"
        //iconPosition: "center"
        text: "Back"
        color: "green"
        onClicked: back();
    }

    Flare
    {
        id: flare

        onShowed:
        {
            hideFlash();
        }
    }

    LoadingScreen
    {
        id: loadingScreen
        visible: true
    }

    Connections
    {
        target: valves

        onVictory:
        {
            levelCompleteDialog.confirmed.connect(back);
            levelCompleteDialog.show(gameField);
        }

        onLose:
        {
            levelFailDialog.confirmed.connect(back);
            levelFailDialog.show(gameField);
        }

        onFlash:
        {
            gameScreen.showFlash();
        }

        onAtmosphereChanged:
        {
            atmosphere.opacity = atmosphereLevel;
            console.log("Atmosphere: " + atmosphere.opacity);
        }
    }
}
