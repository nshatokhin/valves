#-------------------------------------------------
#
# Project created by QtCreator 2013-04-07T23:41:00
#
#-------------------------------------------------

CONFIG += qt plugin

INCLUDEPATH += $$PWD

SOURCES += core/valvesplugin.cpp \
    core/valves.cpp

HEADERS += \
    core/valvesplugin.h \
    core/valves.h \
    core/random_utils.h
