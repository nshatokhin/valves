import QtQuick 2.0
import Ubuntu.Components 0.1

import Valves 1.0

Rectangle {
    id: buttonHolder
    width: units.gu(25)
    height: units.gu(25)
    color: "transparent"

    property int row: 0
    property int column: 0

    property int state: 0

    signal stateUpdated(int _row, int _column, int _state);
    signal buttonClicked(int row, int column);

    signal release;

    /*Button
    {
        id: button
        iconSource: "closed.png"
        iconPosition: "center"
        width: buttonHolder.width
        height: buttonHolder.height
        color: "transparent"

        onClicked:
        {
            console.log("(" + row + ", " + column + ")")
            buttonClicked(row, column)
        }
    }*/
    Rectangle
    {
        id: button
        anchors.centerIn: parent
        anchors.fill: parent
        radius: parent.width/2
        color: Qt.rgba(0,0,0,0.5)

        Image
        {
            id: buttonIcon
            anchors.centerIn: parent
            anchors.fill: parent
            source: "closed.png"
            width: buttonHolder.width
            height: buttonHolder.height
        }
    }

    MouseArea
    {
        anchors.fill: parent

        onClicked:
        {
            console.log("(" + row + ", " + column + ")")
            buttonClicked(row, column)
        }
    }

    onStateUpdated:
    {

        if(row == _row && column == _column)
        {
            state = _state;
        }
    }

    onStateChanged:
    {
        if(state == 0)
            //button.iconSource = "closed.png";
            buttonIcon.source = "closed.png"
        else
            //button.iconSource = "opened.png"
            buttonIcon.source = "opened.png"
    }

    onRelease:
    {
        buttonHolder.destroy(0);
    }
}
