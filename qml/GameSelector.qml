import QtQuick 2.0
import Ubuntu.Components 0.1

import Valves 1.0

Rectangle {
    anchors.fill: parent
    color: "black"

    signal back;
    signal startGame(int width, int height);

    Button
    {
        x: units.gu(1)
        y: units.gu(1)
        //iconSource: "icon.png"
        //iconPosition: "center"
        text: "Back"
        color: "green"
        onClicked: back();
    }

    Column
    {
        anchors.centerIn: parent
        spacing: units.gu(1)

        Row
        {
            id: difficult

            spacing: units.gu(1)

            property int currentDifficult: 4

            onCurrentDifficultChanged: {
                size4x4Btn.checked = currentDifficult == 4
                size6x6Btn.checked = currentDifficult == 6
                size8x8Btn.checked = currentDifficult == 8
                size16x16Btn.checked = currentDifficult == 16
            }

            GraphicCheckButton
            {
                id: size4x4Btn
                //anchors.centerIn: parent
                checked: true

                onClicked:
                {
                    difficult.currentDifficult = 4;
                }
            }

            GraphicCheckButton
            {
                id: size6x6Btn
                //anchors.centerIn: parent

                onClicked:
                {
                    difficult.currentDifficult = 6;
                }
            }

            GraphicCheckButton
            {
                id: size8x8Btn
                //anchors.centerIn: parent

                onClicked:
                {
                    difficult.currentDifficult = 8;
                }
            }

            GraphicCheckButton
            {
                id: size16x16Btn
                //anchors.centerIn: parent

                onClicked:
                {
                    difficult.currentDifficult = 16;
                }
            }
        }

        Row
        {
            id: buttons

            Button
            {
                id: champain
                text: "Champain"

                onClicked:
                {
                    startGame(difficult.currentDifficult, difficult.currentDifficult);
                }
            }
        }
    }
}
